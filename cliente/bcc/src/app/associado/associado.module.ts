import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssociadoComponent } from './associado/associado.component';

@NgModule({
  declarations: [AssociadoComponent],
  imports: [
    CommonModule
  ]
})
export class AssociadoModule { }
